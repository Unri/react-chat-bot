import {
  Card, CardContent, CardMedia, makeStyles, Typography
} from '@material-ui/core';
import React from 'react';
import { buildWeatherApiIconsUrl, Weather } from '../../api/weather';

type WeatherInfoMessageProps = {
  observation: {
    /** Last observation time (YYYY-MM-DD HH:MM). */
    time: string;
    /** City name. */
    city_name: string;
    /**  Country abbreviation. */
    country_code: string;
    /** Temperature (default Celcius). */
    temp: number;
    weather: Weather;
  };
};

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    maxWidth: 540
  },
  icon: {
    maxWidth: 120,
    objectFit: 'contain'
  }
}));

const WeatherInfoMessage = ({
  observation: {
    time,
    city_name,
    country_code,
    weather,
    temp
  }
}: WeatherInfoMessageProps) => {
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardMedia
        component="img"
        className={classes.icon}
        src={buildWeatherApiIconsUrl(weather.icon)}
        title={weather.description}
        height="120"
        width="120"
      />
      <CardContent>
        <Typography component="h5" variant="h5">
          {`${city_name} (${country_code}) ${temp}°C`}
        </Typography>
        <Typography variant="subtitle1" color="textSecondary">
          {weather.description}
        </Typography>
        <Typography variant="subtitle2" color="textSecondary">
          {new Date(time).toLocaleString('en', { hour12: true })}
        </Typography>
      </CardContent>
    </Card>
  );
};
export default WeatherInfoMessage;
