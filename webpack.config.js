const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const DotEnv = require('dotenv-webpack');
const webpack = require('webpack');

const devMode = process.env.NODE_ENV !== 'production';

const htmlWebpackPlugin = new HtmlWebPackPlugin({
  favicon: './src/favicon.ico',
  template: './src/index.html',
  filename: './index.html'
});

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: '[name].css',
  chunkFilename: '[id].css'
});

module.exports = {
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          { loader: 'babel-loader' }
        ]
      },
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(c|sa|sc)ss$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'url-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js']
  },
  devServer: {
    historyApiFallback: true,
    host: '127.0.0.1',
    port: 8080
  },
  output: {
    publicPath: '/',
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    htmlWebpackPlugin,
    miniCssExtractPlugin,
    new DotEnv(),
    new webpack.DefinePlugin({
      'process.env.WEATHER_API_KEY': JSON.stringify(process.env.WEATHER_API_KEY),
      'process.env.SENTRY_DNS': JSON.stringify(process.env.SENTRY_DNS)
    })
  ]
};
