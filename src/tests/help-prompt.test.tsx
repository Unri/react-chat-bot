import React from 'react';
import HelpPrompt from '../components/help-prompt';
import renderWithTheme from './setup-test';

const mockCommands = [

  {
    example: 'cmd1',
    description: 'desc1'
  },
  {
    example: 'cmd2',
    description: 'desc2'
  }
];

describe('HelpPrompt component', () => {
  it('render commands examples in <code> tags', () => {
    const { container } = renderWithTheme(<HelpPrompt commands={mockCommands} />);

    const commandItems = container.querySelectorAll('li');
    commandItems.forEach((cmdItem, i) => {
      expect(cmdItem.textContent).toBe(`${mockCommands[i].example} - ${mockCommands[i].description}`);
      expect(cmdItem.querySelector('code').textContent).toBe(mockCommands[i].example);
    });
  });
});
