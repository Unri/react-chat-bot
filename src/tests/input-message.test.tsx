import React from 'react';
import userEvent from '@testing-library/user-event';
import InputMessage from '../components/input-message';
import renderWithTheme from './setup-test';

describe('InputMessage component', () => {
  it('send with enter key press', () => {
    const onSendSpy = jest.fn();
    const { getByDisplayValue } = renderWithTheme(<InputMessage onSend={onSendSpy} />);

    const domInput = getByDisplayValue('');
    userEvent.type(domInput, 'some message');

    expect(onSendSpy).not.toHaveBeenCalled();
    userEvent.type(domInput, '{enter}');

    expect(onSendSpy).toHaveBeenCalledTimes(1);
  });

  it('send with click on send button', () => {
    const onSendSpy = jest.fn();
    const { getByRole, getByDisplayValue } = renderWithTheme(<InputMessage onSend={onSendSpy} />);

    userEvent.type(getByDisplayValue(''), 'some message');

    expect(onSendSpy).not.toHaveBeenCalled();
    userEvent.click(getByRole('button'));

    expect(onSendSpy).toHaveBeenCalledTimes(1);
  });

  it('don\'t send when no input', () => {
    const onSendSpy = jest.fn();
    const { getByRole, getByDisplayValue } = renderWithTheme(<InputMessage onSend={onSendSpy} />);

    userEvent.type(getByDisplayValue(''), '{enter}');

    expect(getByRole('button')).toBeDisabled();

    expect(onSendSpy).not.toHaveBeenCalled();
  });
});
