// From https://jikan.docs.apiary.io/ (use MyAnimeList.net database)
import qs from 'querystringify';
import { baseSearchUrl, GetMalParams, MalResponse } from './search';

export enum MangaType {
  Manga = 'Manga',
  Novel = 'Novel',
  OneShot = 'One-shot',
  Doujin = 'Doujin',
  Manhwa = 'Manhwa',
  Manhua = 'Manhua'
}

export enum MangaStatus {
  Publishing = 'publishing',
  Completed = 'completed',
  Complete = 'complete',
  ToBePublished = 'to_be_published',
  TBP = 'tbp',
  Upcoming = 'upcoming'
}

export enum MangaOrderBy {
  Title = 'title',
  StartDate = 'start_date',
  EndDate = 'end_date',
  Score = 'score',
  Type = 'type',
  Members = 'members',
  Id = 'id',
  Chapters = 'chapters',
  Volumes = 'Volumes'
}

export enum MangaGenre {
  Action = 1,
  Adventure = 2,
  Cars = 3,
  Comedy = 4,
  Dementia = 5,
  Demons = 6,
  Mystery = 7,
  Drama = 8,
  Ecchi = 9,
  Fantasy = 10,
  Game = 11,
  Hentai = 12,
  Historical = 13,
  Horror = 14,
  Kids = 15,
  Magic = 16,
  MartialArts = 17,
  Mecha = 18,
  Music = 19,
  Parody = 20,
  Samurai = 21,
  Romance = 22,
  School = 23,
  SciFi = 24,
  Shoujo = 25,
  ShoujoAi = 26,
  Shounen = 27,
  ShounenAi = 28,
  Space = 29,
  Sports = 30,
  SuperPower = 31,
  Vampire = 32,
  Yaoi = 33,
  Yuri = 34,
  Harem = 35,
  SliceOfLife = 36,
  Supernatural = 37,
  Military = 38,
  Police = 39,
  Psychological = 40,
  Seinen = 41,
  Josei = 42,
  Doujinshi = 43,
  GenderBender = 44,
  Thriller = 45
}

export interface MangaInfo {
  /** Anime id on MyAnimeList.net */
  mal_id: number;
  /** Url of the anime on MyAnimeList.net */
  url: string;
  image_url: string;
  title: string;
  publishing: boolean;
  synopsis: string;
  type: MangaType;
  chapters: number;
  volumes: number;
  score: number;
  start_date?: Date;
  end_date?: Date;
  /** Number of members of the anime on MyAnimeList.net */
  members: number;
}

export type MangaResponse = MalResponse<MangaInfo>;

export type GetMangaParams = GetMalParams<MangaStatus, MangaGenre, MangaOrderBy>;

const mangaSearchUrl = `${baseSearchUrl}/manga`;

export const searchManga = async (params: GetMangaParams): Promise<MangaInfo[]> => {
  if (params.q.length < 3) throw Error('Please provide at least 3 letters');
  const response: MangaResponse = await fetch(`${mangaSearchUrl}?${qs.stringify(params)}`).then((res) => res.json());
  return response.results;
};
