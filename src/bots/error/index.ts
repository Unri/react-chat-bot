import { MessageHandler } from '../bots.type';
import { User } from '../../reducers/chat/types';

const user: User = {
  name: 'Error',
  isBot: true,
  avatar_url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/97/Dialog-error-round.svg/1200px-Dialog-error-round.svg.png'
};

const messageHandler: MessageHandler = async (_, { author, text }) => {
  if (author.isBot) return;

  const words = text.toString().split(' ');
  const args = words.slice(1).join(' ');

  if (words[0] === 'error') {
    throw new Error(`Error bot sent an error: ${args}`);
  }
};

export default { ...user, messageHandler };
