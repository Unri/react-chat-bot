import { Middleware } from 'redux';
import bots from './bots';
import { ChatActions } from './reducers/chat/actions';

const chatBotMiddleware: Middleware = (store) => (next) => (action) => {
  const result = next(action);
  if (action.type === ChatActions.SEND_MESSAGE) {
    bots.map((bot) => bot.messageHandler(store.dispatch, action.payload));
  }
  return result;
};

export default chatBotMiddleware;
