const randomFoxUrl = 'https://randomfox.ca/floof/';

const getRandomFoxImg = async () => {
  const imgUrl: string = await fetch(randomFoxUrl)
    .then((res) => res.json())
    .then((res) => res.image);
  return imgUrl;
};

export default getRandomFoxImg;
