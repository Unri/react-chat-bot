import { createReducer } from '../helpers';
import { ChatActions } from './actions';
import { User, Message } from './types';

interface ChatReducerState {
  currentUser?: User,
  messages: Message[]
}

export const chatReducerKey = 'chat';

const chatReducer = createReducer<ChatReducerState>({
  [ChatActions.LOGIN_USER]: (state, user: User) => ({
    ...state,
    currentUser: user
  }),
  [ChatActions.SEND_MESSAGE]: (state, message: Message) => ({
    ...state,
    messages: [message, ...state.messages]
  })
}, { messages: [] });

export default chatReducer;
