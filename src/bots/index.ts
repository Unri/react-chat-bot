import { ChatBot } from './bots.type';
import myanimelist from './myanimelist';
import weather from './weather';
import animal from './animal';
import error from './error';

const bots: ChatBot[] = [
  myanimelist,
  weather,
  animal,
  error
];

export default bots;
