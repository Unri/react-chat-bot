import {
  AppBar,
  IconButton,
  makeStyles,
  Toolbar,
  Typography
} from '@material-ui/core';
import React, {
  useCallback,
  useEffect, useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
import clsx from 'clsx';
import { Menu } from '@material-ui/icons';
import ContactList from '../components/contact-list';
import MessageThread from '../components/message-thread';
import { loginUser, sendMessage } from '../reducers/chat/actions';
import { getCurrentUser } from '../reducers/chat/selector';
import bots from '../bots';
import InputMessage from '../components/input-message';

// Mock fetching contacts
const contacts = bots.map(({ name, avatar_url, isBot }) => ({ name, avatar_url, isBot }));

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    position: 'relative',
    maxHeight: 'calc(100% - 3.5rem)',
    [theme.breakpoints.up('sm')]: {
      maxHeight: 'calc(100% - 4rem)'
    }
  },
  menuIcon: {
    [theme.breakpoints.up('md')]: {
      display: 'none'
    }
  },
  main: {
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  aside: {
    backgroundColor: theme.palette.background.default,
    height: `calc(100% - ${theme.spacing(1)})`,
    left: '-100%',
    transition: 'transform 0.8s',
    position: 'absolute',
    width: `calc(100% - ${theme.spacing(1)})`,
    zIndex: 10,
    [theme.breakpoints.up('md')]: {
      transition: 'none',
      transform: 'translateX(0)',
      position: 'initial',
      width: 'initial'
    }
  },
  asideShow: {
    transform: `translateX(calc(100% + ${theme.spacing(1)}))`
  }
}));

const Home = () => {
  const [showContacts, setShowContacts] = useState(false);
  const classes = useStyles();
  const currentUser = useSelector(getCurrentUser);
  const dispatch = useDispatch();

  // Mock user login
  useEffect(() => {
    dispatch(loginUser({ name: 'Me', isBot: false }));
  }, [dispatch]);

  const onSendMessage = useCallback((value: string) => {
    dispatch(sendMessage({
      id: uuidv4(),
      author: currentUser,
      text: value,
      sendDate: new Date()
    }));
  }, [dispatch, currentUser]);

  return (
    <>
      <AppBar position="static" color="transparent">
        <Toolbar>
          <IconButton
            className={classes.menuIcon}
            edge="start"
            aria-label="contacts"
            onClick={() => setShowContacts((bool) => !bool)}
          >
            <Menu />
          </IconButton>
          <Typography variant="h6">Chatbot</Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.root}>
        <aside className={clsx(classes.aside, { [classes.asideShow]: showContacts })}>
          <ContactList contacts={contacts} />
        </aside>
        <div className={classes.main}>
          <MessageThread />
          <InputMessage onSend={onSendMessage} />
        </div>
      </div>
    </>
  );
};

export default Home;
