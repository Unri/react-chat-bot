import { AnyAction, Reducer } from 'redux';

export const createAction = <PayloadType = never>(type: string) => (
  payload: PayloadType
) => ({ type, payload });

export const createReducer = <ReducerState>(
  reducer: {[type: string]: (state: ReducerState, payload) => ReducerState},
  initialState: ReducerState
): Reducer<ReducerState> => (
    state = initialState, { type, payload }: AnyAction
  ) => reducer[type]?.(state, payload) ?? state;
