// eslint-disable-next-line import/no-extraneous-dependencies
import { render } from '@testing-library/react';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import React, { ReactNode } from 'react';
import theme from '../theme';

const renderWithTheme = (node: ReactNode) => render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    {node}
  </ThemeProvider>
);

export default renderWithTheme;
