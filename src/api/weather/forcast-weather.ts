// From https://www.weatherbit.io/api/weather-forecast-120-hour

import { fetchWeather, Weather } from './utils';

interface ForcastWeatherData {
  /** Unix Timestamp at UTC time. */
  ts: number;
  /** Timestamp at local time. */
  timestamp_local: string;
  /** Timestamp at UTC time. */
  timestamp_utc: string;
  /** Wind gust speed (Default m/s). */
  wind_gust_spd: number;
  /** Wind speed (Default m/s). */
  wind_spd: number;
  /** Wind direction (degrees). */
  wind_dir: number;
  /** Abbreviated wind direction. */
  wind_cdir: string;
  /** Verbal wind direction. */
  wind_cdir_full: string;
  /** Temperature (default Celcius). */
  temp: number;
  /** Apparent/"Feels Like" temperature (default Celcius). */
  app_temp: number;
  /** Probability of Precipitation (%). */
  pop: number;
  /** Accumulated liquid equivalent precipitation (default mm). */
  precip: number;
  /** Snowfall (default mm/hr). */
  snow: number;
  /** Snow Depth (default mm). */
  snow_depth: number;
  /** Sea level pressure (mb). */
  slp: number;
  /** Pressure (mb). */
  pres: number;
  /** Relative humidity (%). */
  rh: number;
  /** Low-level (~0-3km AGL) cloud coverage (%). */
  clouds_low: number;
  /** Mid-level (~3-5km AGL) cloud coverage (%). */
  clouds_mid: number;
  /** High-level (>5km AGL) cloud coverage (%). */
  clouds_hi: number;
  /** Cloud coverage (%). */
  clouds: number;
  weather: Weather;
  /** Part of the day (d = day / n = night). */
  pod: string;
  /** UV Index (0-11+). */
  uv: number;
  /** Diffuse horizontal solar irradiance (W/m^2) [Clear Sky]. */
  dhi: number;
  /** Direct normal solar irradiance (W/m^2) [Clear Sky]. */
  dni: number;
  /** Global horizontal solar irradiance (W/m^2) [Clear Sky]. */
  ghi: number;
  /** Estimated Solar Radiation (W/m^2). */
  solar_rad: number;
  /** Visibility (default KM). */
  vis: number;
  /** Average Ozone (Dobson units). */
  ozone: number;
}

interface ForcastWeatherReponse {
  data: ForcastWeatherData[];
  /** Count of returned observations. */
  count: number;
  /** Latitude (Degrees). */
  lat: number;
  /** Longitude (Degrees). */
  lon: number;
  /** Local IANA Timezone. */
  timezone: string;
  /** City name. */
  city_name: string;
  /**  Country abbreviation. */
  country_code: string;
  /** State abbreviation/code. */
  state_code: string;
}

interface ForcastWeatherParams {
  /** Return a specific number of forecast hours. */
  hours?: number
}

const forcastWeatherApiUrl = 'http://api.weatherbit.io/v2.0/forecast/hourly';

export default fetchWeather<ForcastWeatherReponse, ForcastWeatherParams>(forcastWeatherApiUrl);
