import {
  List, ListItem, makeStyles
} from '@material-ui/core';
import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import clsx from 'clsx';
import { getChatMessages, getCurrentUser } from '../reducers/chat/selector';
import MessageText from './message-text';
import UserAvatar from './user-avatar';

const useStyles = makeStyles({
  root: {
    height: '100%',
    padding: 0,
    overflowY: 'auto',
    display: 'flex',
    flexDirection: 'column-reverse',
    scrollBehavior: 'smooth',
    // To hide the scrollbar if an other element overlap
    zIndex: 1
  },
  message: {
    alignItems: 'flex-start'
  },
  userMessage: {
    textAlign: 'right',
    alignSelf: 'flex-end',
    flexDirection: 'row-reverse'
  },
  userAvatar: {
    marginLeft: 'auto'
  }
});

const MessageThread = () => {
  const classes = useStyles();
  const messages = useSelector(getChatMessages);
  const currentUser = useSelector(getCurrentUser);
  const ref = useRef<HTMLUListElement>(null);

  useEffect(() => {
    ref.current?.scrollTo({ behavior: 'smooth', top: 0 });
  }, [messages]);

  return (
    <List ref={ref} className={classes.root}>
      {
        messages.map(({
          id, author, text, sendDate
        }) => {
          const { name, avatar_url: avatarUrl, isBot } = author;
          const isCurrentUser = author === currentUser;
          return (
            <ListItem
              key={id}
              className={clsx(classes.message, {
                [classes.userMessage]: isCurrentUser
              })}
            >
              <UserAvatar
                name={name}
                avatarUrl={avatarUrl}
                className={clsx({
                  [classes.userAvatar]: isCurrentUser
                })}
              />
              <MessageText name={name} isBot={isBot} sendDate={sendDate} isUser={isCurrentUser}>
                {text}
              </MessageText>
            </ListItem>
          );
        })
      }
    </List>
  );
};

export default MessageThread;
