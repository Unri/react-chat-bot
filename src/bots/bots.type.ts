import { Dispatch } from 'redux';
import { User, Message } from '../reducers/chat/types';

export interface MessageHandler {
  (dispatch: Dispatch, message: Message): void | Promise<void>
}

export interface ChatBot extends User {
  messageHandler: MessageHandler
}
