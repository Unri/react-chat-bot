import React from 'react';
import {
  List, ListItem, ListItemText, makeStyles, Typography
} from '@material-ui/core';
import UserAvatar from './user-avatar';
import UserName from './user-name';

interface ContactItem {
  name: string;
  avatar_url?: string;
  isBot?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 240
  },
  heading: {
    paddingLeft: theme.spacing(1)
  }
}));

const ContactList = ({ contacts }: {contacts: ContactItem[]}) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Typography variant="h6" component="h2" className={classes.heading}>Contacts</Typography>
      <List>
        {
          contacts.map(({ name, avatar_url, isBot }) => (
            <ListItem key={name}>
              <UserAvatar name={name} avatarUrl={avatar_url} />
              <ListItemText primary={<UserName name={name} isBot={isBot} />} />
            </ListItem>
          ))
        }
      </List>
    </div>
  );
};

export default ContactList;
