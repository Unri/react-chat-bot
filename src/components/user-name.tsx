import React from 'react';
import { Android } from '@material-ui/icons';
import { makeStyles, Typography } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
  icon: {
    verticalAlign: '-0.3rem',
    marginLeft: theme.spacing(0.5),
    color: theme.palette.text.hint
  }
}));

const UserName = ({ name, isBot }: {name: string; isBot: boolean}) => {
  const classes = useStyle();
  return (
    <Typography variant="body1" component="span">
      {name}
      {isBot && <Android titleAccess="Bot" className={classes.icon} />}
    </Typography>
  );
};

export default UserName;
