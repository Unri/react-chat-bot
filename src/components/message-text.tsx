import React, { FC } from 'react';
import { makeStyles, Theme, Typography } from '@material-ui/core';
import UserName from './user-name';

interface MessageTextProps {
  name: string;
  isBot: boolean;
  sendDate: Date;
  isUser?: boolean;
}

const formatDate = new Intl.DateTimeFormat('en', { dateStyle: 'medium', timeStyle: 'medium' }).format;

const useStyle = makeStyles<Theme, {isUser: boolean}>((theme) => ({
  root: {
    marginTop: '6px',
    marginLeft: (props) => (props.isUser ? theme.spacing(2.5) : ''),
    marginRight: (props) => (!props.isUser ? theme.spacing(2.5) : ''),
    flex: '1 1 auto',
    minWidth: 0
  },
  primary: {
    display: (props) => (props.isUser ? 'flex' : 'block'),
    flexDirection: (props) => (props.isUser ? 'row-reverse' : 'row'),
    margin: 0
  },
  secondary: {
    color: theme.palette.text.secondary,
    display: 'block',
    margin: 0,
    wordBreak: 'break-word',
    whiteSpace: 'break-spaces'
  },
  time: {
    margin: `0 ${theme.spacing(1)}`
  }
}));

const MessageText: FC<MessageTextProps> = ({
  name, isBot, sendDate, isUser = false, children
}) => {
  const classes = useStyle({ isUser });
  return (
    <div className={classes.root}>
      <span className={classes.primary}>
        <b><UserName name={name} isBot={isBot} /></b>
        <Typography
          component="time"
          variant="caption"
          color="textSecondary"
          className={classes.time}
          dateTime={sendDate.toISOString()}
        >
          {formatDate(sendDate)}
        </Typography>
      </span>
      {children && (
        <Typography component="div" variant="body1" className={classes.secondary}>
          {children}
        </Typography>
      )}
    </div>
  );
};

export default MessageText;
