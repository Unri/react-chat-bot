import React, { FC, HTMLAttributes } from 'react';
import { Avatar, ListItemAvatar } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';

interface UserAvatarProps extends HTMLAttributes<HTMLDivElement> {
  name: string;
  avatarUrl?: string;
}

const UserAvatar: FC<UserAvatarProps> = ({ name, avatarUrl = '', className }) => (
  <ListItemAvatar>
    <Avatar className={className} alt={name} src={avatarUrl}>
      {!avatarUrl && <AccountCircle />}
    </Avatar>
  </ListItemAvatar>
);

export default UserAvatar;
