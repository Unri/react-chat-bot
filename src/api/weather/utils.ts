import qs from 'querystringify';

export interface Weather {
  /** Weather icon code. */
  icon: string;
  /** Weather code. */
  code: number;
  /** Text weather description. */
  description: string;
}

interface WeatherRequestParams {
  /** Your API Key. */
  key: string;
  /** Language. */
  lang?: string;
  city: string;
  country?: string;
}

export const buildWeatherApiIconsUrl = (iconCode: string) => `https://www.weatherbit.io/static/img/icons/${iconCode}.png`;

export const fetchWeather = <ResponseType, Params = {}>(url: string) => async ({
  lang = 'en',
  city,
  country
}: Omit<WeatherRequestParams, 'key'> & Params) => {
  if (!city) throw new Error('Please provide a city name');
  const response: ResponseType = await fetch(`${url}?${qs.stringify({
    key: process.env.WEATHER_API_KEY,
    lang,
    city,
    country
  })}`).then((res) => res.json());
  return response;
};
