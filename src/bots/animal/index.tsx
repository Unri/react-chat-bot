import React, { ReactNode } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { getRandomCatImg, getRandomFoxImg } from '../../api/random';
import HelpPrompt from '../../components/help-prompt';
import { sendMessage } from '../../reducers/chat/actions';
import { User } from '../../reducers/chat/types';
import { MessageHandler } from '../bots.type';

const user: User = {
  name: 'Animal',
  isBot: true,
  avatar_url: 'https://vignette.wikia.nocookie.net/animaljam/images/5/53/Fox-icon-png-17.png/revision/latest?cb=20170507171800'
};

const messageHandler: MessageHandler = async (dispatch, { author, text }) => {
  if (author.isBot) return;

  const command = text.toString().split(' ')[0];

  const dispatchMessage = (message: ReactNode) => dispatch(sendMessage({
    id: uuidv4(),
    author: user,
    text: message,
    sendDate: new Date()
  }));

  try {
    if (command === 'cat') {
      const catImgUrl = await getRandomCatImg();
      dispatchMessage(<img src={catImgUrl} alt="cat" width="225" style={{ objectFit: 'contain' }} />);
    }
    if (command === 'fox') {
      const foxImgUrl = await getRandomFoxImg();
      dispatchMessage(<img src={foxImgUrl} alt="fox" width="225" style={{ objectFit: 'contain' }} />);
    }
    if (command === 'help') {
      dispatchMessage(<HelpPrompt commands={[
        {
          example: 'cat',
          description: 'get a random cat image'
        },
        {
          example: 'fox',
          description: 'get a random fox image'
        }
      ]}
      />);
    }
  } catch (e: unknown) {
    dispatchMessage(e.toString());
  }
};

export default { ...user, messageHandler };
