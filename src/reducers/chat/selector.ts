import * as R from 'ramda';
import { Message, User } from './types';
import { chatReducerKey } from './reducer';

export const getCurrentUser = R.path<User>([chatReducerKey, 'currentUser']);

export const getChatMessages = R.path<Message[]>([chatReducerKey, 'messages']);
