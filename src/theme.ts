import { createMuiTheme } from '@material-ui/core';

export default createMuiTheme({
  palette: {
    type: 'dark',
    text: {
      secondary: 'rgba(255, 255, 255, 0.8)'
    }
  },
  spacing: (factor) => `${0.8 * factor}rem`
});
