import { BrowserRouter, Route, Switch } from 'react-router-dom';

import React from 'react';
import { Box } from '@material-ui/core';
import Home from './pages/home';

const Routes = () => (
  <Box height="100vh" display="flex" flexDirection="column">
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Home} exact />
      </Switch>
    </BrowserRouter>
  </Box>
);

export default Routes;
