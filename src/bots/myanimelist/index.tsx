import React, { ReactNode } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { searchAnime, searchManga } from '../../api/myanimelist';
import { sendMessage } from '../../reducers/chat/actions';
import { User } from '../../reducers/chat/types';
import { MessageHandler } from '../bots.type';
import { AnimeInfoCard, MangaInfoCard } from './info-card';
import HelpPrompt from '../../components/help-prompt';

const user: User = {
  name: 'MyAnimeList',
  isBot: true,
  avatar_url: 'https://image.winudf.com/v2/image/bmV0Lm15YW5pbWVsaXN0X2ljb25fMTUyNjk5MjEwNV8wODE/icon.png?w=170&fakeurl=1&type=.png'
};

const messageHandler: MessageHandler = async (dispatch, { author, text }) => {
  if (author.isBot) return;

  const words = text.toString().split(' ');

  const dispatchMessage = (message: ReactNode) => dispatch(sendMessage({
    id: uuidv4(),
    author: user,
    text: message,
    sendDate: new Date()
  }));

  const args = words.slice(1).join(' ');

  try {
    if (words[0] === 'anime') {
      const animes = await searchAnime({ q: args, page: 1 });
      dispatchMessage(animes.length > 0 ? <AnimeInfoCard anime={animes[0]} /> : `Could not found: ${args}`);
    }
    if (words[0] === 'manga') {
      const mangas = await searchManga({ q: args, page: 1 });
      dispatchMessage(mangas.length > 0 ? <MangaInfoCard manga={mangas[0]} /> : `Could not found: ${args}`);
    }
    if (words[0] === 'help') {
      dispatchMessage(<HelpPrompt commands={[
        {
          example: 'anime <name>',
          description: 'search an anime by name'
        },
        {
          example: 'manga <name>',
          description: 'search an manga by name'
        }
      ]}
      />);
    }
  } catch (e: unknown) {
    dispatchMessage(e.toString());
  }
};

export default { ...user, messageHandler };
