import {
  Button, createStyles, OutlinedInput, Theme, WithStyles, withStyles
} from '@material-ui/core';
import { Send } from '@material-ui/icons';
import React, {
  KeyboardEventHandler, useCallback, useRef, useState
} from 'react';

const inputStyle = (theme: Theme) => createStyles({
  inputContainer: {
    display: 'flex',
    flexGrow: 1,
    gap: theme.spacing(1),
    padding: theme.spacing(0.5),
    width: '100%'
  },
  inputMessage: {
    backgroundColor: theme.palette.divider
  },
  inputFieldSet: {
    border: 'none'
  }
});

interface InputMessageProps extends WithStyles<typeof inputStyle> {
  onSend: (value: string) => void;
}

const InputMessage = withStyles(inputStyle)(({ classes, onSend }: InputMessageProps) => {
  const [isEdited, setIsEdited] = useState(false);
  const ref = useRef<HTMLInputElement>(null);

  const handleSendMessage = useCallback(() => {
    const { value } = ref.current;

    onSend(value);

    ref.current.value = '';
    setIsEdited(false);
  }, [onSend]);

  const handleTyping = useCallback<KeyboardEventHandler<HTMLInputElement>>((e) => {
    const input = e.target as HTMLInputElement;

    if (input.value && e.key === 'Enter') handleSendMessage();
  }, [handleSendMessage]);

  return (
    <div className={classes.inputContainer}>
      <OutlinedInput
        inputRef={ref}
        onKeyDown={handleTyping}
        onChange={() => setIsEdited(!!ref.current.value)}
        classes={{
          root: classes.inputMessage,
          notchedOutline: classes.inputFieldSet
        }}
        placeholder="Type here"
        fullWidth
      />
      <Button
        variant="contained"
        color="primary"
        disabled={!isEdited}
        endIcon={<Send />}
        onClick={handleSendMessage}
      >
        Send
      </Button>
    </div>
  );
});

export default InputMessage;
