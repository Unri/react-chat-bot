import { createAction } from '../helpers';
import { User, Message } from './types';

export enum ChatActions {
  LOGIN_USER = 'LOGIN_USER',
  SEND_MESSAGE = 'SEND_MESSAGE'
}

export const loginUser = createAction<User>(ChatActions.LOGIN_USER);

export const sendMessage = createAction<Message>(ChatActions.SEND_MESSAGE);
