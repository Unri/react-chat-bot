import { combineReducers } from 'redux';
import chatReducer, { chatReducerKey } from './chat/reducer';

export default combineReducers({ [chatReducerKey]: chatReducer });
