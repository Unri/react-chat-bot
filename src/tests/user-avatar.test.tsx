import React from 'react';
import UserAvatar from '../components/user-avatar';
import renderWithTheme from './setup-test';

const mockUser = {
  name: 'username',
  avatarUrl: 'some-url'
};

describe('UserAvatar component', () => {
  it('render user avatar img from url', () => {
    const { getByAltText } = renderWithTheme(<UserAvatar
      name={mockUser.name}
      avatarUrl={mockUser.avatarUrl}
    />);

    const avatar = getByAltText(mockUser.name);
    expect(avatar).toHaveAttribute('src', mockUser.avatarUrl);
  });

  it('render default avatar', () => {
    const { container } = renderWithTheme(<UserAvatar name={mockUser.name} />);

    expect(container.querySelector('svg')).toBeInTheDocument();
  });
});
