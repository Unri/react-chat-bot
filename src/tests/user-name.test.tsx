import React from 'react';
import UserName from '../components/user-name';
import renderWithTheme from './setup-test';

describe('UserName component', () => {
  it.each(['bot', 'user'])('render %s name', (userType) => {
    const isBot = userType === 'bot';
    const { getByText, getByTitle } = renderWithTheme(<UserName
      name="username"
      isBot={isBot}
    />);

    expect(getByText('username')).toBeInTheDocument();
    if (isBot) {
      expect(getByTitle('Bot')).toBeInTheDocument();
    }
  });
});
