// From https://jikan.docs.apiary.io/ (use MyAnimeList.net database)

import qs from 'querystringify';
import {
  GetMalParams, MalResponse, baseSearchUrl, Rated
} from './search';

export enum AnimeType {
  Tv = 'TV',
  Ova = 'OVA',
  Movie = 'Movie',
  Special = 'Special',
  Ona = 'ONA',
  Music = 'Music'
}

export enum AnimeStatus {
  Airing = 'airing',
  Completed = 'completed',
  Complete = 'complete',
  ToBeAired = 'to_be_aired',
  TBA = 'tba',
  Upcoming = 'upcoming'
}

export enum AnimeOrderBy {
  Title = 'title',
  StartDate = 'start_date',
  EndDate = 'end_date',
  Score = 'score',
  Type = 'type',
  Members = 'members',
  Id = 'id',
  Episodes = 'episodes',
  Rating = 'rating'
}

export enum AnimeGenre {
  Action = 1,
  Adventure = 2,
  Cars = 3,
  Comedy = 4,
  Dementia = 5,
  Demons = 6,
  Mystery = 7,
  Drama = 8,
  Ecchi = 9,
  Fantasy = 10,
  Game = 11,
  Hentai = 12,
  Historical = 13,
  Horror = 14,
  Kids = 15,
  Magic = 16,
  MartialArts = 17,
  Mecha = 18,
  Music = 19,
  Parody = 20,
  Samurai = 21,
  Romance = 22,
  School = 23,
  SciFi = 24,
  Shoujo = 25,
  ShoujoAi = 26,
  Shounen = 27,
  ShounenAi = 28,
  Space = 29,
  Sports = 30,
  SuperPower = 31,
  Vampire = 32,
  Yaoi = 33,
  Yuri = 34,
  Harem = 35,
  SliceOfLife = 36,
  Supernatural = 37,
  Military = 38,
  Police = 39,
  Psychological = 40,
  Thriller = 41,
  Seinen = 42,
  Josei = 43
}

export interface AnimeInfo {
  /** Anime id on MyAnimeList.net */
  mal_id: number;
  /** Url of the anime on MyAnimeList.net */
  url: string;
  image_url: string;
  title: string;
  airing: boolean;
  synopsis: string;
  type: AnimeType;
  episodes: number;
  score: number;
  start_date: Date;
  end_date?: Date;
  /** Number of members of the anime on MyAnimeList.net */
  members: number;
  rated: Rated;
}

export type AnimeResponse = MalResponse<AnimeInfo>;

export type GetAnimeParams = GetMalParams<AnimeStatus, AnimeGenre, AnimeOrderBy>;

const animeSearchUrl = `${baseSearchUrl}/anime`;

export const searchAnime = async (params: GetAnimeParams): Promise<AnimeInfo[]> => {
  if (params.q.length < 3) throw Error('Please provide at least 3 letters');
  const response: AnimeResponse = await fetch(`${animeSearchUrl}?${qs.stringify(params)}`).then((res) => res.json());
  return response.results;
};
