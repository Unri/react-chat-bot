import { ReactNode } from 'react';

export interface User {
  name: string;
  avatar_url?: string;
  isBot: boolean;
}

export interface Message {
  id: string;
  author: User,
  text: ReactNode,
  sendDate: Date
}
