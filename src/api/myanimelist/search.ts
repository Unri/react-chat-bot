export interface MalResponse<ResultInfo> {
  request_hash: string;
  request_cached: boolean;
  request_cache_expiry: number;
  results: ResultInfo[];
  last_page: number;
}

export interface ErrorResponse {
  status: number;
  /** Exception type */
  type: string;
  message: string;
  error: string;
}

export enum Sort {
  Ascending = 'ascending',
  Asc = 'asc',
  Descending = 'descending',
  Desc = 'desc'
}

/** Rating */
export enum Rated {
  /** All ages */
  G = 'G',
  /** Children */
  PG = 'PG',
  /** Teens 13 or older */
  PG13 = 'PG-13',
  /** 17+ recommended (violence & profanity) */
  R17 = 'R17',
  /** Mid nuddity (may also contain violence & profanity) */
  R = 'R',
  /** Hentai (extreme sexual content/nudity) */
  RX = 'Rx'
}

export interface GetMalParams<Status, Genre, OrderBy> {
  page: number;
  /** Query string to search */
  q: string;
  type?: string;
  status?: Status;
  rated?: Rated;
  genre?: Genre;
  /** Score (float between 0.0-10.0) */
  score?: number;
  /** Start date (format: yyy-mm-dd) */
  start_date?: string;
  /** End date (format: yyy-mm-dd) */
  end_date?: string;
  /** To exclude/include the `genre` you added in your request */
  genre_exclude?: boolean;
  limit?: number;
  order_by?: OrderBy;
  sort?: Sort;
  /** Producer id */
  producer?: number;
  /** Magazine id */
  magazine?: number;
  /** Search by the letter it start with */
  letter?: string;
}

export const baseSearchUrl = 'https://api.jikan.moe/v3/search';
