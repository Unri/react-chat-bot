import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import thunk from 'redux-thunk';
import chatBotMiddleware from './middlewares';
import reducers from './reducers';

const productMode = (env) => {
  const enhancers = applyMiddleware(thunk, chatBotMiddleware);
  return createStore(reducers, (env !== 'production'
    ? composeWithDevTools(enhancers)
    : compose(enhancers)
  ));
};

export default productMode(process.env.NODE_ENV);
