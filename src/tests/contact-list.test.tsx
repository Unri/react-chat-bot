import React from 'react';
import ContactList from '../components/contact-list';
import renderWithTheme from './setup-test';

const mockContacts = [
  {
    name: 'Bot 1',
    avatar_url: 'bot-1-avatar-url',
    isBot: true
  },
  {
    name: 'Bot 2',
    avatar_url: 'bot-2-avatar-url',
    isBot: true
  },
  {
    name: 'User 1',
    avatar_url: 'user-1-avatar-url',
    isBot: false
  }
];

describe('ContactList component', () => {
  it('render without error', () => {
    const {
      getByText,
      getAllByTitle,
      getByAltText,
      container
    } = renderWithTheme(<ContactList contacts={mockContacts} />);
    const botItems = container.querySelectorAll('li');

    expect(getByText('Contacts')).toBeInTheDocument();
    expect(getByText('User 1')).toBeInTheDocument();
    expect(botItems.length).toBe(3);
    expect(getAllByTitle('Bot').length).toBe(2);
    expect(getByAltText(mockContacts[1].name)).toHaveAttribute('src', mockContacts[1].avatar_url);
  });
});
