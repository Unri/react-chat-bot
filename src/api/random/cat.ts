const randomCatUrl = 'https://aws.random.cat/meow';

const getRandomCatImg = async () => {
  const imgUrl: string = await fetch(randomCatUrl)
    .then((res) => res.json())
    .then((res) => res.file);
  return imgUrl;
};

export default getRandomCatImg;
