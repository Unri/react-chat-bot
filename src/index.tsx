// Dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { CssBaseline, ThemeProvider } from '@material-ui/core';
import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';

import store from './store';

import './style.css';

import Routes from './routes';
import theme from './theme';

Sentry.init({
  dsn: process.env.SENTRY_DNS,
  release: `react-chat-bot@${process.env.npm_package_version}`,
  integrations: [new Integrations.BrowserTracing()],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0
});

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Routes />
    </ThemeProvider>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app'));
