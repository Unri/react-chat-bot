import React from 'react';

interface Command {
  example: string;
  description: string;
}

const HelpPrompt = ({ commands }: {commands: Command[]}) => (
  <>
    <span>Commands:</span>
    <ul>
      {commands.map(({ example, description }) => (
        <li key={example.split(' ')[0]}>
          <code>{example}</code>
          {` - ${description}`}
        </li>
      ))}
    </ul>
  </>

);

export default HelpPrompt;
