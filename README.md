# React chat bot

This is a school project means for practicing React with Redux.

React chat bot is a front-end chatroom with some bots linked to APIs. These bots respond to specific command messages.

## Run the project

To run this project, you need to add an environment variable `WEATHER_API_KEY`. It's an key for the [weatherbit](https://www.weatherbit.io/) API.
You can get one from registering [here](https://www.weatherbit.io/account/create).
You can use `.env` file to run the project locally.
