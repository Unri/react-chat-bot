import {
  Card, CardContent, CardMedia, Link, makeStyles, Typography
} from '@material-ui/core';
import React from 'react';
import { AnimeInfo, MangaInfo } from '../../api/myanimelist';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 540,
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row'
    }
  },
  image: {
    objectFit: 'contain',
    padding: theme.spacing(1.5),
    margin: theme.spacing(0, 'auto'),
    width: 200
  },
  title: {
    color: theme.palette.info.main
  },
  description: {
    padding: theme.spacing(2, 0)
  }
}));

interface InfoCardProps {
  title: string;
  title_url: string;
  image_url: string;
  description: string;
  additionnalInfos: Record<string, string | number>;
}

const InfoCard = ({
  title,
  title_url,
  image_url,
  description,
  additionnalInfos = {}
}: InfoCardProps) => {
  const classes = useStyles();
  return (
    <Card variant="outlined" className={classes.root}>
      <CardMedia
        component="img"
        alt={`${title} image`}
        className={classes.image}
        image={image_url}
        title={`${title} image`}
        width="200"
      />
      <CardContent>
        <Link href={title_url} target="_blank" rel="noreferrer" variant="h5" className={classes.title}>
          {title}
        </Link>
        {description && (
          <Typography variant="body2" component="p" className={classes.description}>
            {description}
          </Typography>
        )}
        <div>
          {
            Object.entries(additionnalInfos).map(([key, value]) => (
              <Typography key={`${title} ${key}`} variant="body1" component="p">
                <b>
                  {key}
                  :
                </b>
                {' '}
                {value}
              </Typography>
            ))
          }
        </div>
      </CardContent>
    </Card>
  );
};

export const AnimeInfoCard = ({
  anime: {
    title, url, image_url, episodes, score, synopsis, type, airing
  }
}: { anime: AnimeInfo }) => (
  <InfoCard
    title={`${title} (${type})`}
    title_url={url}
    image_url={image_url}
    description={synopsis}
    additionnalInfos={{
      Score: `${score}/10`,
      Episodes: episodes,
      Status: airing ? 'Currently Airing' : 'Finished Airing'
    }}
  />
);

export const MangaInfoCard = ({
  manga: {
    title,
    url,
    image_url,
    synopsis,
    chapters,
    score,
    type,
    publishing,
    volumes
  }
}: { manga: MangaInfo }) => (
  <InfoCard
    title={`${title} (${type})`}
    title_url={url}
    image_url={image_url}
    description={synopsis}
    additionnalInfos={{
      Score: `${score}/10`,
      Volumes: volumes,
      Chapters: chapters,
      Published: publishing ? 'Publishing' : 'Finished'
    }}
  />
);
