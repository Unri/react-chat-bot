import React, { ReactNode } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { getCurrentWeather, getForcastWeather } from '../../api/weather';

import { MessageHandler } from '../bots.type';
import { User } from '../../reducers/chat/types';
import WeatherInfoMessage from './weather-info-message';
import { sendMessage } from '../../reducers/chat/actions';
import HelpPrompt from '../../components/help-prompt';

const user: User = {
  name: 'Weather',
  isBot: true,
  avatar_url: 'https://www.weatherbit.io/static/img/icons/c02d.png'
};

const messageHandler: MessageHandler = async (dispatch, { author, text }) => {
  if (author.isBot) return;

  const words = text.toString().split(' ');

  const dispatchMessage = async (message: ReactNode) => dispatch(sendMessage({
    id: uuidv4(),
    author: user,
    text: message,
    sendDate: new Date()
  }));

  const args = words.slice(1).join(' ');

  try {
    if (words[0] === 'weather') {
      const observations = await getCurrentWeather({ city: args });
      observations?.data.forEach(({ ob_time, ...observation }) => (
        dispatchMessage(<WeatherInfoMessage observation={{
          time: ob_time,
          ...observation
        }}
        />)
      ));
    }
    if (words[0] === 'forcast') {
      const observations = await getForcastWeather({ city: args, hours: 24 });
      const nextDayObservation = observations?.data[23];
      dispatchMessage(<WeatherInfoMessage observation={{
        city_name: observations.city_name,
        country_code: observations.country_code,
        time: nextDayObservation.timestamp_local,
        ...nextDayObservation
      }}
      />);
    }
    if (words[0] === 'help') {
      dispatchMessage(<HelpPrompt commands={[
        {
          example: 'weather <city name>',
          description: 'get the current/last observed weather'
        },
        {
          example: 'forcast <city name>',
          description: 'get tomorrow\'s meteo'
        }
      ]}
      />);
    }
  } catch (e) {
    dispatchMessage(e.toString());
  }
};

export default { ...user, messageHandler };
